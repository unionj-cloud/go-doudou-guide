module seed

go 1.15

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/unionj-cloud/go-doudou v0.8.3
)